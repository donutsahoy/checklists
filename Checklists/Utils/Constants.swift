//
//  Constants.swift
//  Checklists
//
//  Created by rjs on 2/21/22.
//

import Foundation

struct Constants {
    struct UserDefaultKeys {
        static let ChecklistIndex = "ChecklistIndex"
        static let FirstTime = "FirstTime"
    }
    
    struct ChecklistIcons {
        static let Appointments = "Appointments"
        static let Birthdays = "Birthdays"
        static let Chores = "Chores"
        static let Drinks = "Drinks"
        static let Folder = "Folder"
        static let Groceries = "Groceries"
        static let Inbox = "Inbox"
        static let NoIcon = "No Icon"
        static let Photos = "Photos"
        static let Trips = "Trips"
    }
    
    static let iconsArr = [
        ChecklistIcons.NoIcon, ChecklistIcons.Appointments, ChecklistIcons.Birthdays, ChecklistIcons.Chores,
        ChecklistIcons.Drinks, ChecklistIcons.Folder, ChecklistIcons.Groceries, ChecklistIcons.Inbox, ChecklistIcons.Photos, ChecklistIcons.Trips
    ]

}
