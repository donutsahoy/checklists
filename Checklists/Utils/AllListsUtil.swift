//
//  AllListsUtil.swift
//  Checklists
//
//  Created by rjs on 2/22/22.
//

import Foundation

struct AllListsUtil {
    static func createSubTitleString(_ checklist: ChecklistModel) -> String {
        var returnStr = ""
        let count = checklist.countUncheckedItems()
        if checklist.items.count == 0 {
            returnStr = "(No Items)"
        } else {
            returnStr = count == 0 ? "All Done" : "\(count) Remaining"
        }
        
        return returnStr
    }
}
