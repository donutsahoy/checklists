//
//  AllListsCell.swift
//  Checklists
//
//  Created by rjs on 2/22/22.
//

import UIKit

class AllListsCell: UITableViewCell {

    var viewModel: AllListsCellViewModel? {
        didSet {
            configure()
        }
    }
    private var title: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Arial", size: 18)
        return label
    }()
    
    private var subTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Arial", size: 12)
        return label
    }()
    
    private var icon: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        
        return iv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    
    func configure() {
        addSubview(icon)
        icon.centerY(inView: self)
        icon.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, paddingLeft: 6)
        
        let stack = UIStackView(arrangedSubviews: [title, subTitle])
        stack.alignment = .leading
        stack.distribution = .fillEqually
        stack.axis = .vertical
        addSubview(stack)
        stack.centerY(inView: self)
        stack.anchor(top: self.topAnchor, left: icon.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingLeft: 12)
        
        self.accessoryType = .detailDisclosureButton
        
        title.text = viewModel?.title
        subTitle.text = viewModel?.subTitle
        icon.image = viewModel?.iconImage
    }
}
