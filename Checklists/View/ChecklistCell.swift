//
//  ChecklistCell.swift
//  Checklists
//
//  Created by rjs on 2/11/22.
//

import Foundation
import UIKit

class ChecklistCell: UITableViewCell {
    
    var viewModel: ChecklistCellViewModel? {
        didSet {
            configure()
        }
    }
    
    private let checkmark = UIImage(systemName: "checkmark")
    private let checklistItemLabel: UILabel = {
        let l = UILabel()
        return l
    }()
    
    private lazy var checkmarkImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.tintColor = .tintColor
        return iv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.tag = 1
        
        addSubview(checkmarkImage)
        checkmarkImage.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: 12)
        
        addSubview(checklistItemLabel)
        checklistItemLabel.centerY(inView: self, leftAnchor: checkmarkImage.rightAnchor, paddingLeft: 8)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        guard let viewModel = viewModel else { return }

        checklistItemLabel.text = viewModel.labelText
        self.accessoryType = .detailDisclosureButton
        if (viewModel.isChecked) {
            checkmarkImage.image = checkmark
        } else {
            checkmarkImage.image = nil
        }
    }
    
}
