//
//  AddItemCell.swift
//  Checklists
//
//  Created by rjs on 2/15/22.
//

import Foundation
import UIKit

protocol AddItemCellDelegate: AnyObject {
    func updateTextVal(_ str: String?) -> Void
    func wantsToComplete(_ str: String?) -> Void
}

class AddItemCell: UITableViewCell {
    
    //MARK: - Properties
    weak var delegate: AddItemCellDelegate?
    
    lazy var itemInput: UITextField = {
        let tf = UITextField(frame: .zero)
        tf.placeholder = "Name of the Item"
        tf.addTarget(self, action: #selector(checkIfPopulated), for: .editingChanged)
        tf.addTarget(self, action: #selector(input), for: .editingDidEndOnExit)
        tf.font = UIFont(name: "System", size: 17)
        tf.adjustsFontSizeToFitWidth = false
        tf.autocapitalizationType = .sentences
        tf.returnKeyType = .done
        tf.enablesReturnKeyAutomatically = true
        return tf
    }()
    
    //MARK: - Actions
    @objc func input() {
        self.delegate?.wantsToComplete(itemInput.text)
    }
    
    @objc func checkIfPopulated() {
        self.delegate?.updateTextVal(itemInput.text)
    }
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView() {
        contentView.addSubview(itemInput)
        itemInput.centerY(inView: self, leftAnchor: self.leftAnchor, paddingLeft: 8)
        itemInput.becomeFirstResponder()
    }
}
