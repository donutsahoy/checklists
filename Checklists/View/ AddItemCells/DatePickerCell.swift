//
//  DatePickerCell.swift
//  Checklists
//
//  Created by rjs on 3/5/22.
//

import UIKit

protocol DatePickerCellDelegate: AnyObject {
    func didUpdateDateTime(_ controller: DatePickerCell, date: Date)
}

class DatePickerCell: UITableViewCell {

    // MARK: - Properties
    weak var delegate: DatePickerCellDelegate?
    
    private let label: UILabel = {
        let label = UILabel()
        label.text = "Due Date"
        return label
    }()
    
    lazy var datePicker: UIDatePicker = {
        let dp = UIDatePicker()
        dp.datePickerMode = .dateAndTime
        dp.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        return dp
    }()
    
    // MARK: - Actions
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
        delegate?.didUpdateDateTime(self, date: sender.date)

    }
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    func configureView() {
        contentView.addSubview(label)
        label.centerY(inView: self)
        label.anchor(left: self.leftAnchor, paddingLeft: 6)
        
        contentView.addSubview(datePicker)
        datePicker.centerY(inView: self)
        datePicker.anchor(right: self.rightAnchor, paddingRight: 6)
    }
    
}
