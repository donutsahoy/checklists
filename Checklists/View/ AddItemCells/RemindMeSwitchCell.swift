//
//  RemindMeSwitchCell.swift
//  Checklists
//
//  Created by rjs on 3/3/22.
//

import UIKit

protocol RemindMeSwitchCellDelegate: AnyObject {
    func onSwitchUpdate(_ controller: RemindMeSwitchCell, isSwitchOn: Bool)
}

class RemindMeSwitchCell: UITableViewCell {

    // MARK: - Properties
    weak var delegate: RemindMeSwitchCellDelegate?
    
    private let label: UILabel = {
        let label = UILabel()
        label.text = "Remind Me"
        return label
    }()
    
    lazy var remindSwitch: UISwitch = {
        let s = UISwitch()
        s.addTarget(self, action: #selector(onSwitch(_:)), for: .valueChanged)
        return s
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView() {
        contentView.addSubview(label)
        label.centerY(inView: self)
        label.anchor(left: self.leftAnchor, paddingLeft: 6)
        
        contentView.addSubview(remindSwitch)
        remindSwitch.centerY(inView: self)
        remindSwitch.anchor(right: self.rightAnchor, paddingRight: 6)
    }
    
    // MARK: - Actions
    @objc func onSwitch(_ sender: UISwitch) {
        delegate?.onSwitchUpdate(self, isSwitchOn: sender.isOn)
    }
}
