//
//  ChecklistViewModel.swift
//  Checklists
//
//  Created by rjs on 2/16/22.
//

import Foundation

class ChecklistViewModel {
    var checklist: ChecklistModel? {
        didSet {
            configure()
        }
    }

    var pageTitle = ""
    
    init(withChecklist checklist: ChecklistModel) {
        self.checklist = checklist
    }
    
    func configure() {
        guard let checklist = checklist else { return }
        self.pageTitle = checklist.name
    }
}
