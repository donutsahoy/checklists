//
//  ChecklistCellViewModel.swift
//  Checklists
//
//  Created by rjs on 2/11/22.
//

import Foundation

struct ChecklistCellViewModel {
    var labelText: String
    
    var isChecked: Bool = false
    init(item: CheckListItemModel) {
        labelText = item.itemLabel
        isChecked = item.isItemChecked
    }
}
