//
//  AllListsIconCellViewModel.swift
//  Checklists
//
//  Created by rjs on 2/23/22.
//

import Foundation
import UIKit

struct AllListsIconCellViewModel {
    var checklistName: String
    var iconImage: UIImage
    var iconImageName: String
    
    init(withChecklist checklist: ChecklistModel) {
        self.checklistName = checklist.name
        self.iconImageName = checklist.iconName
        self.iconImage = UIImage(imageLiteralResourceName: checklist.iconName)
    }
}
