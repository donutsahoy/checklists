//
//  viewModel.swift
//  Checklists
//
//  Created by rjs on 2/22/22.
//

import Foundation
import UIKit

struct AllListsCellViewModel {
    
    var title: String
    var subTitle: String
    var iconImage: UIImage
    
    init(withChecklist checklist: ChecklistModel) {
        self.title = checklist.name
        self.subTitle = AllListsUtil.createSubTitleString(checklist)
        self.iconImage = UIImage(imageLiteralResourceName: checklist.iconName)
    }
    

}
