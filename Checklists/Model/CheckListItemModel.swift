//
//  CheckListItemModel.swift
//  Checklists
//
//  Created by rjs on 2/14/22.
//

import Foundation
import UserNotifications

class CheckListItemModel: NSObject, Codable {
    var itemLabel: String
    var isItemChecked: Bool
    
    var dueDate = Date()
    var shouldRemind = false
    var itemID: UUID
    
    init(itemLabel: String, isItemChecked: Bool) {
        self.itemLabel = itemLabel
        self.isItemChecked = isItemChecked
        self.itemID = UUID()
    }
    
    init(itemLabel: String, isItemChecked: Bool, shouldRemind: Bool, dueDate: Date) {
        self.itemID = UUID()
        self.itemLabel = itemLabel
        self.isItemChecked = isItemChecked
        self.shouldRemind = shouldRemind
        self.dueDate = dueDate
    }
    
    deinit {
        removeNotification()
    }
        
    func scheduleNotification() {
        removeNotification()
      if shouldRemind && dueDate > Date() {
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Reminder:"
        content.body = itemLabel
        content.sound = UNNotificationSound.default

        // 2
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(
          [.year, .month, .day, .hour, .minute],
          from: dueDate)
        // 3
        let trigger = UNCalendarNotificationTrigger(
          dateMatching: components,
          repeats: false)
        // 4
        let request = UNNotificationRequest(
          identifier: "\(itemID)",
          content: content,
          trigger: trigger)
        // 5
        let center = UNUserNotificationCenter.current()
        center.add(request)

        print("Scheduled: \(request) for itemID: \(itemID)")
      }
    }

    func removeNotification() {
      let center = UNUserNotificationCenter.current()
      center.removePendingNotificationRequests(withIdentifiers: ["\(itemID)"])
    }
}
