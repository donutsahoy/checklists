//
//  ChecklistModel.swift
//  Checklists
//
//  Created by rjs on 2/17/22.
//

import Foundation

class ChecklistModel: NSObject, Codable {
    var name = ""
    
    var iconName = Constants.ChecklistIcons.NoIcon
    
    var items = [CheckListItemModel]()
    
    init(name: String, iconName: String = Constants.ChecklistIcons.NoIcon) {
        self.name = name
        super.init()
    }
    
    func countUncheckedItems() -> Int {
        let unCheckedItems = items.filter { item in
            return !item.isItemChecked
        }
        return unCheckedItems.count
    }
}
