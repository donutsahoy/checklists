//
//  ItemDetailViewController.swift
//  Checklists
//
//  Created by rjs on 2/14/22.
//

import UIKit

private let reuseIdentifier = "addItem"
private let reuseIdentifier1 = "shouldRemind"
private let reuseIdentifier2 = "dueDate"

protocol ItemDetailViewControllerDelegate: AnyObject {
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController)
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishAdding item: CheckListItemModel)
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: CheckListItemModel)
}

class ItemDetailViewController: UITableViewController {
    
    //MARK: - Properties
    private var itemString: String?
    
    private var shouldRemind: Bool?
    
    private var dueDate: Date?
    
    var itemToEdit: CheckListItemModel?
    
    weak var delegate: ItemDetailViewControllerDelegate?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureTableView()
        configureNavigation()
        if let item = itemToEdit {
            title = "Edit Item"
            itemString = item.itemLabel
            shouldRemind = item.shouldRemind
            dueDate = item.dueDate
        }
    }
    
    //MARK: - Actions
    
    @objc func onSave() {
        if let itemString = itemString {
            if let itemToEdit = itemToEdit {
                itemToEdit.itemLabel = itemString
                itemToEdit.shouldRemind = shouldRemind ?? false
                itemToEdit.dueDate = dueDate ?? Date()
                itemToEdit.scheduleNotification()
                
                delegate?.itemDetailViewController(self, didFinishEditing: itemToEdit)
            } else {
                let item = CheckListItemModel(itemLabel: itemString, isItemChecked: false, shouldRemind: shouldRemind ?? false, dueDate: dueDate ?? Date())
                item.scheduleNotification()
                
                delegate?.itemDetailViewController(self, didFinishAdding: item)
            }
        } else {
            delegate?.itemDetailViewControllerDidCancel(self)
        }
        
    }
    
    @objc func onClose() {
        delegate?.itemDetailViewControllerDidCancel(self)
    }
    
    //MARK: - Helpers
    
    func configureNavigation() {
        navigationItem.title = "Add Item"
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onSave))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onClose))
        navigationItem.rightBarButtonItem?.isEnabled = itemToEdit != nil ? true : false
    }
    
    func configureTableView() {
        tableView.register(AddItemCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.register(RemindMeSwitchCell.self, forCellReuseIdentifier: reuseIdentifier1)
        tableView.register(DatePickerCell.self, forCellReuseIdentifier: reuseIdentifier2)
        tableView.rowHeight = 48
    }
}

//MARK: - UITableViewDataSource
extension ItemDetailViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! AddItemCell
            cell.delegate = self
            cell.itemInput.text = itemString
            return cell
        } else if indexPath.section == 1 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier1, for: indexPath) as! RemindMeSwitchCell
            cell.delegate = self
            cell.remindSwitch.isOn = shouldRemind ?? false
            return cell
        } else if indexPath.section == 1 && indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier2, for: indexPath) as! DatePickerCell
            cell.delegate = self
            cell.datePicker.date = dueDate ?? Date()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
            return cell
        }
    }
    
}

//MARK: - UITableViewDelegate
extension ItemDetailViewController {
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
}

//MARK: - AddItemCellDelegate
extension ItemDetailViewController: AddItemCellDelegate {
    func updateTextVal(_ str: String?) {
        if let str = str {
            itemString = str
            navigationItem.rightBarButtonItem?.isEnabled = !str.isEmpty
        }
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func wantsToComplete(_ str: String?) {
        itemString = str
        onSave()
    }
}

// MARK: - RemindMeSwitchCellDelegate
extension ItemDetailViewController: RemindMeSwitchCellDelegate {
    func onSwitchUpdate(_ controller: RemindMeSwitchCell, isSwitchOn: Bool) {
        shouldRemind = isSwitchOn
    }
}

// MARK: - DatePickerCellDelegate
extension ItemDetailViewController: DatePickerCellDelegate {
    func didUpdateDateTime(_ controller: DatePickerCell, date: Date) {
        dueDate = date
    }
}
