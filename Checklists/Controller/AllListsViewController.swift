//
//  AllListsViewController.swift
//  Checklists
//
//  Created by rjs on 2/17/22.
//

import UIKit

private let cellIdentifier = "ChecklistCell"

class AllListsViewController: UITableViewController {
    
    var viewModel = AllListsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Checklist"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAddButtonPress))
        viewModel.loadChecklists()
        viewModel.registerDefaults()
        viewModel.handleFirstTime()
        configureTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.delegate = self
        
        let index = viewModel.indexOfSelectedChecklist
        if index >= 0 && index < viewModel.lists.count {
            let checklist = viewModel.lists[index]
            let newVc = ChecklistViewController()
            newVc.delegate = self
            newVc.viewModel = ChecklistViewModel(withChecklist: checklist)
            
            navigationController?.pushViewController(newVc, animated: true)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func configureTable() {
        tableView.register(AllListsCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    @objc func onAddButtonPress() {
        let newVc = ListDetailViewController(style: .grouped)
        newVc.delegate = self
        navigationController?.pushViewController(newVc, animated: true)
    }
    
}

extension AllListsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.lists.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AllListsCell
        
        cell.viewModel = AllListsCellViewModel(withChecklist: viewModel.lists[indexPath.row])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checklist = viewModel.lists[indexPath.row]
        let newVC = ChecklistViewController()
        newVC.viewModel = ChecklistViewModel(withChecklist: checklist)
        newVC.delegate = self
        
        viewModel.indexOfSelectedChecklist = indexPath.row
        
        navigationController?.pushViewController(newVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        viewModel.lists.remove(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let controller = ListDetailViewController()
        controller.delegate = self
        controller.checklistToEdit = viewModel.lists[indexPath.row]
        
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension AllListsViewController: ListDetailViewControllerDelegate {
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdding checklist: ChecklistModel) {
        viewModel.lists.append(checklist)
        
        viewModel.sortChecklists()
        tableView.reloadData()
        
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing checklist: ChecklistModel) {
        viewModel.sortChecklists()
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
}

extension AllListsViewController: ChecklistViewControllerDelegate {
    func didUpdateChecklist(_ controller: ChecklistViewController, checklist: ChecklistModel) {
        if let index = viewModel.lists.firstIndex(of: checklist) {
            viewModel.lists[index] = checklist
        }
    }
}

extension AllListsViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController === self {
            viewModel.indexOfSelectedChecklist = -1
        }
    }
}
