//
//  ListDetailViewController.swift
//  Checklists
//
//  Created by rjs on 2/17/22.
//

import UIKit

// FIXME: Not picking up if icon image is left at default
protocol ListDetailViewControllerDelegate: AnyObject {
    func listDetailViewControllerDidCancel(
        _ controller: ListDetailViewController)
    
    func listDetailViewController(
        _ controller: ListDetailViewController,
        didFinishAdding checklist: ChecklistModel
    )
    
    func listDetailViewController(
        _ controller: ListDetailViewController,
        didFinishEditing checklist: ChecklistModel
    )
}

class ListDetailViewController: UITableViewController, UITextFieldDelegate {
    //MARK: - Properties
    private let checklistNameTextField: UITextField = {
        let tf = UITextField(frame: .zero)
        tf.font = UIFont(name: "System", size: 17)
        tf.adjustsFontSizeToFitWidth = false
        tf.autocapitalizationType = .sentences
        tf.returnKeyType = .done
        tf.enablesReturnKeyAutomatically = true
        tf.addTarget(self, action: #selector(done), for: .editingDidEndOnExit)
        return tf
    }()
    
    private let iconLabel: UILabel = {
        let il = UILabel()
        il.text = "Icon"
        return il
    }()
    
    private lazy var iconImage: UIImageView = {
        let iv = UIImageView(image: UIImage(imageLiteralResourceName: Constants.ChecklistIcons.Appointments))
        return iv
    }()
    
    private var iconName: String = ""
    
    weak var delegate: ListDetailViewControllerDelegate?
    
    var checklistToEdit: ChecklistModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checklistNameTextField.becomeFirstResponder()
    }
    
    func configure() {
        title = "Add Checklist"
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        
        if let checklist = checklistToEdit {
            title = "Edit Checklist"
            checklistNameTextField.text = checklist.name
            iconName = checklist.iconName
            iconImage.image = UIImage(imageLiteralResourceName: iconName)
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    @objc func cancel() {
        delegate?.listDetailViewControllerDidCancel(self)
    }
    
    @objc func done() {
        if let checklist = checklistToEdit {
            checklist.name = checklistNameTextField.text!
            delegate?.listDetailViewController(
                self,
                didFinishEditing: checklist)
        } else {
            let checklist = ChecklistModel(name: checklistNameTextField.text!, iconName: iconName)
            delegate?.listDetailViewController(
                self,
                didFinishAdding: checklist)
        }
    }
}

//MARK: - Table View Delegates
extension ListDetailViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(frame: .zero)
        if (indexPath.section == 0) {
            cell.addSubview(checklistNameTextField)
            checklistNameTextField.fillSuperview()
            checklistNameTextField.anchor(paddingLeft: 6, paddingRight: 6)
        } else if indexPath.section == 1 {
            cell.addSubview(iconLabel)
            iconLabel.centerY(inView: cell)
            iconLabel.anchor(left: cell.leftAnchor, right: cell.rightAnchor, paddingLeft: 6, paddingRight: 6)
            
            cell.addSubview(iconImage)
            iconImage.centerY(inView: cell)
            iconImage.anchor(right: cell.rightAnchor, paddingRight: 32)
            
            cell.accessoryType = .disclosureIndicator
        }
        
        
        return cell
    }
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath.section == 1 ? indexPath : nil
    }
    
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        let oldText = textField.text!
        let stringRange = Range(range, in: oldText)!
        let newText = oldText.replacingCharacters(
            in: stringRange,
            with: string)
        navigationItem.rightBarButtonItem?.isEnabled = !newText.isEmpty
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        navigationItem.rightBarButtonItem?.isEnabled = false
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let newVC = IconPickerViewController()
            newVC.delegate = self
            navigationController?.pushViewController(newVC, animated: true)
        }
    }
}
 // MARK: - IconPickerViewControllerDelegate
extension ListDetailViewController: IconPickerViewControllerDelegate {
    func iconPicker(_ picker: IconPickerViewController, didPick iconName: String) {
        iconImage.image = UIImage(imageLiteralResourceName: iconName)
        checklistToEdit?.iconName = iconName
        self.iconName = iconName
    }
    
}
