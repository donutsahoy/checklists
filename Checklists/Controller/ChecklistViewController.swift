//
//  ViewController.swift
//  Checklists
//
//  Created by rjs on 2/11/22.
//

import UIKit

private let reuseIdentifier = "CheckListItem"

protocol ChecklistViewControllerDelegate: AnyObject {
    func didUpdateChecklist(_ controller: ChecklistViewController, checklist: ChecklistModel)
}
class ChecklistViewController: UITableViewController {
    //MARK: - Properties
    var viewModel: ChecklistViewModel? {
        didSet {
            configure()
        }
    }
    
    weak var delegate: ChecklistViewControllerDelegate?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAddButtonPress))
    }
    
    func configureTableView() {
        view.backgroundColor = .white
        tableView.register(ChecklistCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.rowHeight = 64
    }
    
    @objc func onAddButtonPress() {
        let newVc = ItemDetailViewController(style: .grouped)
        newVc.delegate = self
        navigationController?.pushViewController(newVc, animated: true)
    }
    
    func configure() {
        guard let viewModel = viewModel else { return }

        configureTableView()
        navigationItem.title = viewModel.pageTitle
    }
}

//MARK: - UITableViewDataSource
extension ChecklistViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.checklist?.items.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ChecklistCell
        guard let checklist = viewModel?.checklist else { return cell }
        cell.viewModel = ChecklistCellViewModel(item: checklist.items[indexPath.row])
        return cell
    }
}

//MARK: - UITableViewDelegate
extension ChecklistViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ChecklistCell {
            cell.viewModel?.isChecked.toggle()
            viewModel?.checklist?.items[indexPath.row].isItemChecked.toggle()
            if let checklist = viewModel?.checklist {
                delegate?.didUpdateChecklist(self, checklist: checklist)
            }
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        viewModel.checklist?.items.remove(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        let newVC = ItemDetailViewController()
        newVC.delegate = self
        
        let item = viewModel?.checklist?.items[indexPath.row]
        newVC.itemToEdit = item
        
        
        navigationController?.pushViewController(newVC, animated: true)
    }
}

extension ChecklistViewController: ItemDetailViewControllerDelegate {
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishAdding item: CheckListItemModel) {
        guard let viewModel = viewModel else { return }
        let newRowIndex = viewModel.checklist?.items.count
        viewModel.checklist?.items.append(item)
        
        if let newRowIndex = newRowIndex {
            let indexPath = IndexPath(row: newRowIndex, section: 0)
            let indexPaths = [indexPath]
            tableView.insertRows(at: indexPaths, with: .automatic)
            
            if let checklist = viewModel.checklist {
                delegate?.didUpdateChecklist(self, checklist: checklist)
            }
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: CheckListItemModel) {
        guard let viewModel = viewModel else { return }
        if let index = viewModel.checklist?.items.firstIndex(of: item) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) as? ChecklistCell {
                cell.viewModel = ChecklistCellViewModel(item: item)
                if let checklist = viewModel.checklist {
                    delegate?.didUpdateChecklist(self, checklist: checklist)
                }
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
}
